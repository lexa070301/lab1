package impls

import interfaces.Functions

/**
 * Класс, реализующий интерфейс Functions
 */
class FunctionsImpl : Functions {
    override fun calculate(a: Int, b: Int) = a + b
    override fun substringCounter(s: String, sub: String): Int {
        // Из длины строки вычитаем длину этой строки после удаления всех подстрок и делим на длину подстроки
        return (s.length - s.replace(sub, "").length) / sub.length;
    }

    override fun splitterSorter(s: String, sub: String): List<String> {
        // Разделяем строку по подстроке и превращаем в список
        return s.split(sub).toList()
    }

    override fun uniqueCounter(s: String, sub: String): Map<String, Int> {
        // Разделяем строку по подстроке, групперуем и для каждого элемента считаем количество
        return s.split(sub).groupingBy{
            it
        }.eachCount()
    }

    override fun isPalindrome(s: String): Boolean {
        // Проверяем на пустоту строки, пробигаем циклом до середины строки и сравниваем символы с начала с
        // символами с конца, если все совпадают, то возвращаем true, если нет false
        if (s == "") return false

        var bool: Boolean = true

        for (i in 0..s.length / 2) {
            if (s[i] != s[s.length - i - 1]) {
                bool = false;
                break
            }
        }

        return bool;
    }

    override fun invert(s: String): String {
        // метод .reversed() поэтому пробигаем по строке циклом и перемещаем символы наоборот
        var result: String = s

        for (i in 0..s.length - 1) {
            result = result.toCharArray().also{
                it[i] = s[s.length - i - 1]
            }.joinToString("")
        }

        return result
    }
}
